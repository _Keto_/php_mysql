<?php
    class BankAccount {
        private $balance;

        public function deposit($amount) {
            $this->balance += $amount;
        }

        public function withdraw($amount) {
            if ($amount <= $this->balance) {
                $this->balance -= $amount;
                return $amount;
            } else {
                echo "Insufficient funds";
                return 0;
            }
        }

        public function getBalance() {
            return $this->balance;
        }
    }
?>