<?php
    class Animal {
        public function eat() {
            echo "Animal is eating";
        }
    }

    class Dog extends Animal {
        public function bark() {
            echo "Dog is barking";
        }
    }

    $dog = new Dog();
    $dog->eat();
    $dog->bark();
?>