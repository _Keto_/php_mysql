<?php
    class Animal {
        public function makeSound() {
            echo "Animal sound";
        }
    }

    class Dog extends Animal {
        public function makeSound() {
            echo "Woof";
        }
    }

    class Cat extends Animal {
        public function makeSound() {
            echo "Meow";
        }
    }

    function animalSound(Animal $animal) {
        $animal->makeSound();
    }

    $dog = new Dog();
    $cat = new Cat();

    animalSound($dog);
    animalSound($cat); 

?>
