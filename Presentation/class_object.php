<?php
    class Car {
        public $brand;
        public $model;
        
        public function displayInfo() {
            echo "Brand: " . $this->brand . ", Model: " . $this->model;
        }
    }

    $car1 = new Car();
    $car1->brand = "Toyota";
    $car1->model = "Camry";
    $car1->displayInfo();
?>