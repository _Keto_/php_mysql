<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 7</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <nav>
        <li><a href="">Home</a></li>
        <li><a href="">Quiz</a></li>
        <li><a href="">Subject</a></li>
        <li><a href="">File and folder</a></li>
        <li><a href="">Upload file</a></li>
    </nav>
    <div class="content">
        <?php
            include "index.php"
        ?>
    </div>
</body>
</html>